var webapp = angular.module('webapp', ['ui.router','ng-webcam','angularUtils.directives.dirPagination','ui.bootstrap','ui.bootstrap.datetimepicker']);

   webapp.config(function($stateProvider, $urlRouterProvider) {

       $urlRouterProvider.otherwise('/login');

       $stateProvider

       // HOME STATES AND NESTED VIEWS ========================================
           .state('register', {
               url: '/register',
               templateUrl: 'templates/register.html',
               controller:'userController'

           })

           .state('webcam', {
              url: '/webcam',
              templateUrl: 'templates/webcam.html',
              controller:'webcamController'
           })

           .state('member', {
             url: '/member',
             templateUrl: 'templates/familyWebcam.html',
             controller:'memberController'
           })

           .state('table', {
             url: '/table/:registerId',
             templateUrl: 'templates/table.html',
             controller:'tableController'
           })

           .state('tableView', {
               url: '/tableView',
               templateUrl: 'templates/table.html',
               controller:'tableController'
           })

           .state('familytable', {
            url: '/familytable/:memberId',
            templateUrl: 'templates/familytable.html',
            controller:'familytableController'
            })

            .state('familyupdate', {
                url: '/familyupdate/:memberId',
                templateUrl: 'templates/updatefamilytable.html',
                controller:'familyupdateController'
            })

           .state('updatetable', {
                url: '/updatetable/:registerId',
                templateUrl: 'templates/updatetable.html',
                controller:'updateTableController'
            })

            .state('livevideo', {
                url: '/livevideo',
                templateUrl: 'templates/livevideo.html',
               /* controller:'liveVideoController'*/
            })

            .state('settings', {
                url: '/settings',
                templateUrl: 'templates/settings.html',
                controller:''
            })

            .state('systemsettings', {
                url: '/systemsettings',
                templateUrl: 'templates/systemsettings.html',
                controller:'systemSettingController'
            })

            .state('adminsettings', {
                url: '/adminsettings',
                templateUrl: 'templates/adminsettings.html',
                controller:'adminSettingController'
            })

            .state('reports', {
                url: '/reports',
                templateUrl: 'templates/reports.html',
                controller:'queryController'
            })

           // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
           .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller:'userController'

               // we'll get to this in a bit
           });

   });

angular.module('webapp').directive('exportTable', function () {
    var link = function ($scope, element, attr) {
        $scope.$on('export-pdf',function(e,d){
            element.exportDetails({ type:'pdf', escape: false });
        })

        $scope.$on('export-excel', function(e,d){
            element.exportDetails({ type:'excel', escape: false });
        });
        $scope.$on('export-doc',function(e,d){
            element. exportDetails({ type:'doc', escape: false});
        });
        $scope.$on('export-csv', function(e,d){
            element. exportDetails({ type:'csv', escape:false });
        });
    }
    return {
        restrict:'A',
        link:link
    };
});