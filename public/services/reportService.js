webapp.factory("reportService", function ($http) {
    var saveAnalysied = function (data) {
        return $http.post('/storeAnalysiedData', data);
    }

    var getEmotions = function (emotion) {
        return $http.get('/getEmotions/' + emotion);
    }
   var getGender = function (gender) {
        return $http.get('/getGender/' + gender);
    }
    var getAgeRange = function (low,high) {
        return $http.get('/getAgeRange/' + low+high);
    }
    var getDataBasedOnDateRange = function (fromDate,toDate) {
        return $http.get('/getDataBasedOnDateRange/' + fromDate+toDate);
    }
    var getMouthOpenCandidates = function (confidenceFlag) {
        return $http.get('/getMouthOpenCandidates/' + confidenceFlag);
    }
    var getEyesOpenCandidates = function (confidenceFlag) {
        return $http.get('/getEyesOpenCandidates/' + confidenceFlag);
    }
    var getEyeglassesCandidates = function (confidenceFlag) {
        return $http.get('/getEyeglassesCandidates/' + confidenceFlag);
    }

    return {
        saveAnalysied: saveAnalysied,
        getEmotions:getEmotions,
        getGender:getGender,
        getAgeRange:getAgeRange,
        getDataBasedOnDateRange:getDataBasedOnDateRange,
        getMouthOpenCandidates:getMouthOpenCandidates,
        getEyesOpenCandidates:getEyesOpenCandidates,
        getEyeglassesCandidates:getEyeglassesCandidates


    }


})