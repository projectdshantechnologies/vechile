webapp.factory('userServices',function($http){

    var postAllRegisterDetails = function(data)
    {
       return $http.post('/userRegistration', data);
    };

    var postAllLoginDetails = function(data)
    {
        console.log("testing:::::", data)
       return $http.post('/login', data);
    };
    var getAllAdminSettings=function()
                   {
                      return $http.get('/allAdminSetting');
                   }
           var postAdminSettings=function(data)
                    {
                       return $http.post('/adminSetting',data);
                    }
           var deleteAdmin=function(adminId)
                 {
                    return $http.delete('/adminBymongoId/'+adminId);
                 }

          var getSingleAdmin =function(adminId)
                    {
                       return $http.get('/adminBymongoId/'+adminId);
                    }
          var updateAdmin=function(editdata)
                       {
                          return $http.post('/editAdminBymongoId',editdata);
                       }
          var postSystemSettings=function(data)
                       {
                          return $http.post('/systemSetting',data);
                       }
          var deleteSystem=function(systemId)
                {
                   return $http.delete('/systemBymongoId/'+systemId);
                }
          var getAllSystemSettings=function()
                      {
                         return $http.get('/allSystemSetting');
                      }
          var getSingleSystem =function(systemId)
                   {
                      return $http.get('/systemBymongoId/'+systemId);
                   }
          var updateSystem=function(editdata)
                          {
                             return $http.post('/editSystemBymongoId',editdata);
                          }

          var postReportSettings=function(data)
                             {
                                return $http.post('/reportSetting',data);
                             }
            var deleteReport=function(routeId)
                  {
                     return $http.delete('/reportBymongoId/'+routeId);
                  }
            var getAllReportSettings=function()
                        {
                           return $http.get('/allReportSetting');
                        }
            var getSingleReport =function(routeId)
                     {
                        return $http.get('/reportBymongoId/'+routeId);
                     }
            var updateReport=function(editdata)
                            {
                               return $http.post('/editReportBymongoId',editdata);
                            }

    var compareImages=function(photos)
    {
        var photo={
            details:photos
        }
        console.log("***************photo srvice")
        console.log(photo)
        return $http.post('/checkFacialDetails',photo);

    }

    return {
      postAllRegisterDetails:postAllRegisterDetails,
      postAllLoginDetails: postAllLoginDetails,
      getAllAdminSettings: getAllAdminSettings,
     postAdminSettings: postAdminSettings,
     deleteAdmin: deleteAdmin,
     getSingleAdmin: getSingleAdmin,
     updateAdmin:updateAdmin,
     postSystemSettings: postSystemSettings,
     deleteSystem: deleteSystem,
     getAllSystemSettings: getAllSystemSettings,
     getSingleSystem: getSingleSystem,
     updateSystem:updateSystem,
     postReportSettings:postReportSettings,
     deleteReport:deleteReport,
     getAllReportSettings: getAllReportSettings,
     getSingleReport:getSingleReport,
     updateReport:updateReport,
        compareImages:compareImages
    }

});
