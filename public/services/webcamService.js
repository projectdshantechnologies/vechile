webapp.factory("webcamservice", function ($http) {
 var photoss=[];
 var setwebcameimages=function(photos)
 {
for(var i=0;i<photos.length;i++)
{
photoss=photos[i];
//console.log(photoss)
}
//console.log(photoss)
//return $http.post('/imageslist',photoss);
 }
 var getimages=function()
 {
 return photoss;
 }

 var getRegister=function(registerId)
     {
        return $http.get('/registerBymongoId/'+registerId);
     }

 var postAllRegisterDetails = function(data)
     {
        return $http.post('/Registration',data);
     }
 var deleteRegister=function(registerId)
      {
         return $http.delete('/registerBymongoId/'+registerId);
      }
 var updateRegister=function(editdata)
     {
        return $http.post('/editRegisterBymongoId',editdata);
     }
 var getAllRegister=function()
     {
        return $http.get('/AllRegisterDetails');
     }
 var postAllMemberDetails = function(data)
      {
         return $http.post('/Members',data);
      }
 var getMember=function(memberId)
      {
         return $http.get('/memberBymongoId/'+memberId);
      }
 var getAllMember=function()
      {
         return $http.get('/allMemberDetails');
      }
 var deleteMember=function(memberId)
       {
          return $http.delete('/memberBymongoId/'+memberId);
       }
 var updateMember=function(editdata)
      {
         return $http.post('/editMemberBymongoId',editdata);
      }

return{
     setwebcameimages:setwebcameimages,
     getimages:getimages,
     postAllRegisterDetails:postAllRegisterDetails,
     getRegister:getRegister,
     deleteRegister:deleteRegister,
     updateRegister:updateRegister,
     getAllRegister:getAllRegister,
     postAllMemberDetails:postAllMemberDetails,
     getMember: getMember,
     getAllMember:getAllMember,
     deleteMember:deleteMember,
     updateMember:updateMember
  }


})