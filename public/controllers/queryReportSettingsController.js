webapp.controller('queryController', queryController);
queryController.$inject = ['$scope', '$log', 'reportService', '$stateParams', '$state'];
function queryController($scope, $log, reportService, $stateParams, $state) {
    $scope.names = ["Male","Female"];
    $scope.expressions = ["CONFUSED","CALM","SURPRISED","HAPPY"];

    $scope.exportAction = function (option) {
        switch (option) {
            case 'pdf': $scope.$broadcast('export-pdf', {});
                break;
            case 'excel': $scope.$broadcast('export-excel', {});
                break;
            case 'doc': $scope.$broadcast('export-doc', {});
                break;
            case 'csv': $scope.$broadcast('export-csv', {});
                break;
            default: console.log('no event caught');
        }
    }


    $scope.qurieAnalysiedData=[]
    $scope.getAllDataBasedOnParameter=function(details) {
        console.log("***********getAllDataBasedOnParameter**********************")
        console.log(details)

        if(details.gender){
            $scope.qurieAnalysiedData=[]
            reportService.getGender(details.gender).then(function (res) {
                $scope.qurieAnalysiedData = res.data;
            });

        }

        if(details.expressions){
            $scope.qurieAnalysiedData=[]
            reportService.getEmotions(details.expressions).then(function (res) {
                $scope.qurieAnalysiedData = res.data;
            });

        }

        if(details.fromDate &&details.toDate ){
            $scope.qurieAnalysiedData=[]
            reportService.getDataBasedOnDateRange(details.fromDate,details.toDate).then(function (res) {
                $scope.qurieAnalysiedData = res.data;
            });

        }

        if(details.startRange &&details.endRange ){
            $scope.qurieAnalysiedData=[]
            reportService.getAgeRange(details.startRange,details.endRange).then(function (res) {
                $scope.qurieAnalysiedData = res.data;
            });

        }

    }

};