var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var EventResultSchema = new mongoose.Schema(
  {
    name:String,
    phone:Number,
    email:String,
    relationshipWithFamily:String,
    photo:[]
},
{collection:"members"});
mongoose.model('members',EventResultSchema);