/**
 * Created by Suhas on 7/5/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var facialAnalysisSchema = new mongoose.Schema({details:{}},{collection:"facialAnalysisConfiguration"});

module.exports = mongoose.model('facialAnalysisConfiguration',facialAnalysisSchema);