/**
 * Created by dhanalakshmi on 30/5/18.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var UserPaymentDetailsSchema = new mongoose.Schema(
    {
        firstName:String,
        LastName:String,
        phNumber:String,
        email:String,
        rechargeAmt:Number
    },
    {collection:"UserPaymentDetails"});
mongoose.model('UserPaymentDetails',UserPaymentDetailsSchema);
