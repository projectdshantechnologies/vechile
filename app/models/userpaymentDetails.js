/**
 * Created by dhanalakshmi on 30/5/18.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserCardPaymentDetailsSchema = new mongoose.Schema(
    {
        money:String,
        cardNumbe:String,
        expriyDate:String,
        email:String,
        cvv:String,
        name:String
    },
    {collection:"paymentDetails"});
mongoose.model('paymentDetails',UserCardPaymentDetailsSchema);

