/**
 * Created by zendynamix on 07-03-2016.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config');
var hotelDetailsDB = require('../models/hotelDetails.js');
Schema = mongoose.Schema;
mongoose.connect(config.db);
var hotelDetails = [


    {
        "hotelName": "The Lalit Ashok",
        "hotelAddress": "Kumara Krupa High Grounds, Bengaluru560001, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "Hotel Ramanashree Richmond Circle",
        "hotelAddress": "16 Rajaram Mohan Roy Road, Bengaluru 560025, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "Northern Suites",
        "hotelAddress": "No 1623 Aecs Layout 'e' Block | Brookefields, Bengaluru 560037, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "Casa Cottage",
        "hotelAddress": "2 Casa Cottage Clapham Street | Richmond Town, Bengaluru 560025, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 8002 Sri Sai Inn",
        "hotelAddress": "97/1 Varthur Main Road | near Brand Factory, Bengaluru 560037, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 8476 Garden Comfort",
        "hotelAddress": "263/15 11th Cross Wilson Garden, Bengaluru 560027, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "Otel Getfresh",
        "hotelAddress": "No 34 P and T colony 2nd cross R T Nagar Main Road, Bengaluru 560032, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 9251 Grand Krishna",
        "hotelAddress": "Plot No.112, G K Temple Street | Opposite Vijayalaxmi Theater, Bengaluru 560053, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "Magaji Orchid",
        "hotelAddress": "Nagappa Street, Bengaluru 560020, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "OYO 5367 near Outer Ring Road",
        "hotelAddress": "Plot No. 428, 3rd B Main, Kalyan Nagar | Behind Mega Mart, Bengaluru 560043, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "Hotel Kadamba Guestline",
        "hotelAddress": "Kengeri Mysore Road, Bengaluru 560068, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 9925 The Ocea International",
        "hotelAddress": "No 72,Next to St.Joseph's College of Commerce Brigade Road | Markham Road, Bengaluru 560025, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "Lords Eco Inn Jayanagar",
        "hotelAddress": "157 - 158 10'th A Main Road | 1st Block, Behind E Zone, Ashoka Pillar, Bengaluru 560011, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 2132 Apartment Aditya Residency",
        "hotelAddress": "Plot No. 107, 5 A Cross, HRBR Layout, 3rd Block | Kalyan Nagar, Bengaluru 560043, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "Hotel Kanthi Comforts",
        "hotelAddress": "22 2'nd Main Road | near Kanishka Hotel, Bengaluru 560009, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "Golf Inn",
        "hotelAddress": "No 102 Karnataka Golf Association Road | Off intermediate Ring Road, Domlur, Bengaluru 560071, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "OYO 2507 Anugraha Homes",
        "hotelAddress": "Amarjyoti Layout | Domlur, Bengaluru 560071, India",
        "city": "BANGLORE"
    },

    {
        "hotelName": "FabHotel K.R Inn",
        "hotelAddress": "No. 7, 2nd Cross, Banaswadi Ring Road | Horamavu Junction, Bengaluru 560043, India",
        "city": "BANGLORE"
    }

    ,
    {
        "hotelName": "The Krishna Nibbana",
        "hotelAddress": "# 125, Kundalahalli | Brookefield Main Road, Bengaluru 560037, India",
        "city": "BANGLORE"
    },







    {
        "hotelName": "Royal Orchid Metropole Hotel",
        "hotelAddress": "#6 of 128 Hotels in Mysuru (MysORE)",
        "city": "MYSORE"
    },

    {
        "hotelName": "FabHotel Dawn Mysore",
        "hotelAddress": "15 Mysore Bangalore Road | Mysore Bangalore Rd, Bannimantap,, Mysuru (Mysore) 570015, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Pan Pacific Jal Mahal Resort & Spa",
        "hotelAddress": "118/1 Mysore Bannur Road | Varuna Hobli, Mysuru (Mysore) 570011, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Hotel Lok Sagar",
        "hotelAddress": "Plot No. 268 | Jaganmohan Palace Circle, Mysuru (Mysore) 570024, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Ginger Mysore",
        "hotelAddress": "Nazarabad Mohalla Vasanth Mahal Road | Near Nazarabad Police Station, Mysuru (Mysore) 570010, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Hotel Roopa",
        "hotelAddress": "Bangalore Nilgiri Road | Opp. Woodlands Theatre, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Hotel SCVDS",
        "hotelAddress": "	Sri Harsha Road, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Hotel Comforts",
        "hotelAddress": "#259, Ramavilas Vilas Road, Mysuru (Mysore) 570004, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Vertical Garden Hostel",
        "hotelAddress": "#1106,10th Main,11th Cross, Mysuru (Mysore) 570002, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "OYO 7386 The Oasis Hotel",
        "hotelAddress": "5 Plot No M&O 75 - K Part - 1 Hootagalli Industrial Area, Mysuru (Mysore) 570018, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "SK Elegance",
        "hotelAddress": "#2722/1 Sri Harsha Road | near Woodlands Theater, Lashkar Mohalla, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Hotel Sunshine",
        "hotelAddress": "#354 Ashoka Road | Near St. Philomena's Church, Ashoka Road, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Hotel Palace Plaza",
        "hotelAddress": "Harsha Road, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Villa Park",
        "hotelAddress": "#25, Vasantha Mahal Road, | Opp. Chamundi Vihar Stadium Nazarbad, Mysuru (Mysore) 570011, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Hotel Mayura Yatrinivas Mysore",
        "hotelAddress": "2, Jhansi Laxmi Bai Road, Mysuru (Mysore) 570005, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "OYO 1951 Hotel Kings L Grand",
        "hotelAddress": "65 M.G. Road, K.R. Mohalla, opp JSS New Hospital, Mysuru (Mysore) 570004, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Tulips Homestay",
        "hotelAddress": "#922, 6th Main, 10th Cross | Vijayanagar 1st Stage (near Corporation Bank), Mysuru (Mysore) 570017, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Mannars Yatrinivas",
        "hotelAddress": "2747 Hallada Keri East Cross Road | Near Sangam Theatre, Mysuru (Mysore) 570001, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "Olive Residency",
        "hotelAddress": "#783/3. 229/4, Mahajana Layout Abhishek Road Hebbal | ( Near Vijaya Nagar 2nd Stage Water Tank), Mysuru (Mysore) 570017, India",
        "city": "MYSORE"
    },

    {
        "hotelName": "OYO 11535 Sri Sai Comforts",
        "hotelAddress": "122/A, 3rd Main Road, Bannimantap, Mysuru (Mysore) 570015, India",
        "city": "MYSORE"
    }

    ,
    {
        "hotelName": "Hotel Shastri Paradise",
        "hotelAddress": "Opposite. Suburb Bus Stand, Mysuru (Mysore), India",
        "city": "MYSORE"
    },







    {
        "hotelName": "Rajarajeshwari Guest House",
        "hotelAddress": "Munishwara Temple Road | Behind Hotel Radarshan, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Coorg Trekkers Paradise",
        "hotelAddress": "General Thimmaiah Circle, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "Sukhruthi Homes",
        "hotelAddress": "Kadagadal Village | Opposite High School Road, Madikeri 571248, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Taj Madikeri Resort & Spa, Coorg",
        "hotelAddress": "1st Monnangeri, Galibeedu Post, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Hotel Green Haven",
        "hotelAddress": "Kohinoor Road, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "Chirpy Blooms",
        "hotelAddress": "Ulithala Street | Hakathoor village and post, Madikeri 571252, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Coorg Coffee Country Homestay",
        "hotelAddress": "Kakkabe Palace Road, Madikeri 571214, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Leisure Vacations Maithili Home Comforts",
        "hotelAddress": "Near Canara Bank West | Collage Road, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "Field Side Homestay - Kagodlu Post",
        "hotelAddress": "| Makeri Village Kagodlu Post, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Sayuri",
        "hotelAddress": "Munishwara Temple Road, Near Town Hall, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Suriya Homestay",
        "hotelAddress": "Near Birappa Temple, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "La Flora Jungle Hill Resort",
        "hotelAddress": "| Mekeri Village, Kodagu District, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Rainforest Retreat Eco-lodg",
        "hotelAddress": "Mojo Plantation | Galibeedu Village, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Leisure Vacations Nature's Cradle",
        "hotelAddress": "Mangalore Road, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "Euro Valley",
        "hotelAddress": "Katakeri Village, Made Post, Madikeri, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Hotel Madikeri Heritage",
        "hotelAddress": "Daswal Cross Road | Near Pvt. Bus Stand, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Misty Mercara",
        "hotelAddress": "Beera Temple Road, Kannanda Bane, Mahadevpet End, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,
    {
        "hotelName": "OYO 3009 Coorg Mandarin",
        "hotelAddress": "Mysore Road, Opposite Bus Depot, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Hotel Al Wesal International",
        "hotelAddress": "Tower Kohinoor Road | Near K.S.R.T.C. Bus Stand, Madikeri 571201, India",
        "city": "MADIKERI"
    },

    {
        "hotelName": "Highton Hotel",
        "hotelAddress": "Junior College Road, Madikeri 571201, India",
        "city": "MADIKERI"
    }

    ,





    {
        "hotelName": "Ocean Retreat Service Apartments",
        "hotelAddress": "Someshwar Temple Road | MUDA Layout, Mangalore 575024, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "The Gateway Hotel Old Port Rd Mangalore",
        "hotelAddress": "Old Port Road | Opposite to the District Commissioner's Office, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "anani Homes",
        "hotelAddress": "Prema Layout # 4 - 147/30 Binary Homes Kottara Cross Road | Kottara Chowki,, Mangalore 575006, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "Taj Mahal Boarding & Lodging",
        "hotelAddress": "Hampankatta, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "The Ocean Pearl",
        "hotelAddress": "Navabharath circle | Kodialbail, Mangalore 575003, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Hotel BMS",
        "hotelAddress": "Nh 66 | Kunthikana Derebail, Ashok Nagar Post, Mangalore 575006, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "Goldfinch Mangalore",
        "hotelAddress": "Bunts Hostel Road | Near Jyothi Circle, Mangalore 575003, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Ginger Hotel Mangalore",
        "hotelAddress": "Kottara Chowki Junction, Mangalore 575006, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Mangalore Beach Resort",
        "hotelAddress": "Beach Road Beach Road | Someshwar Uchil, Mangalore 575021, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "OYO 10631 Hotel Roopa",
        "hotelAddress": "Balmatta Road, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Hotel Deepa Comforts",
        "hotelAddress": "Mahatma Gandhi Road | Kodialbail, Mangalore 575003, India",
        "city": "MANGALOREE"
    },

    {
        "hotelName": "Hotel Poonja International",
        "hotelAddress": "K S Rao Road | Hampankatta, Mangalore 575001, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "Vasantha Mahal Hotel",
        "hotelAddress": "K S R Road | Dist:Dakshina Kannada, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Falnir Palace",
        "hotelAddress": "Falnir A N 1'st Cross Road | near Platinum Theatre, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Treebo Pappilon Palace",
        "hotelAddress": "| Navabharath Circle, Kodialbail, Mangalore 575003, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "Grand Plaza Hotel",
        "hotelAddress": "Light House Hill Road | Hampankatta, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Soorya Comforts",
        "hotelAddress": "Door No: 2-17-1478/13 & 18, Near KSRTC Bus Stand | Opp KMC Medical College, Kapikad Road, Mangalore 575004, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Metro Plaza Hotel",
        "hotelAddress": "Attavar Road, near Railway Station, Mangalore 575001, India",
        "city": "MANGALORE"
    }

    ,
    {
        "hotelName": "Woodside Hotel",
        "hotelAddress": "Next to Don Bosco Hall | next to Don Bosco Hall, behind Bharath Motors Bldg, Mangalore 575001, India",
        "city": "MANGALORE"
    },

    {
        "hotelName": "Hotel Ayush International",
        "hotelAddress": "Opp Invenger, Mangalore 575003, India",
        "city": "MANGALORE"
    },





    {
        "hotelName": "Hotel Dhammangi Comforts",
        "hotelAddress": "P.B. Road | Opp to Old KSRTC Bus-Stand, Hubli-Dharwad 580029, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Samrat Ashok Hotel",
        "hotelAddress": "Lamington Road, Hubli-Dharwad 580020, India",
        "city": "BHUBLI-DHARWAD"
    },

    {
        "hotelName": "Bharati Lodge",
        "hotelAddress": "P B Road, Near Old Bus Stand, Hubli-Dharwad 580009, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Denissons Hotel",
        "hotelAddress": "110 Dollors Colony, Airport Road, Hubli-Dharwad 580030, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "The President Hotel, Hubli",
        "hotelAddress": "#194/1A, Opp. Unakal Lake | Srinagara Cross, P.B. Road, Hubli-Dharwad 580 031, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Hotel Naveen",
        "hotelAddress": "| Unkal Lake, Hubli-Dharwad 580025, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Clarks Inn Airport Hotel",
        "hotelAddress": "Gokul Road, Hubli-Dharwad 580030, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Mayur Aaditya Resort",
        "hotelAddress": "P B Road | Dharwad, Hubli-Dharwad 580009, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "The Hans Hotel",
        "hotelAddress": "	Vidyanagar P B Road, Hubli-Dharwad 580031, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Cotton County Resort Managed by the Verda",
        "hotelAddress": "Opp Hubli Airport District Dharward, Hubli-Dharwad, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Hotel Metropolis",
        "hotelAddress": "Koppikar Road, Hubli-Dharwad 580020, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "The Verda Dwarawata",
        "hotelAddress": "Hubli - Dharwad Highway | Sadhunavar Hospitality, Near University of Agriculture Sciences, Hubli-Dharwad 580005, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Prn Travel Inn Hote",
        "hotelAddress": "Pune-Bangalore Road | Sattur, Hubli-Dharwad 580002, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Revankar Comforts",
        "hotelAddress": "T.B. Revankar Kalyan Mantap Road | Opposite ICICI Bank, Hubli-Dharwad, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "The Ocean Pearl Resort And Spa",
        "hotelAddress": "Rayapur | Opposite Sanjeevini Park, Rayapur Hubli Healthy County Resort, Hubli-Dharwad 580009, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Hotel Peacock",
        "hotelAddress": "New Cotton Market | Near VRL office, Hubli-Dharwad 580029, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Travel Inn",
        "hotelAddress": "Hubli-Dharwad High Way SDM Dental College,, Hubli-Dharwad 580002, India",
        "city": "HUBLI-DHARWAD"
    }

    ,
    {
        "hotelName": "Hotel Mandhar Regency",
        "hotelAddress": "College Road, Hubli-Dharwad 580001, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Hotel Ashoka Towers",
        "hotelAddress": "Lamington Road, Hubli-Dharwad 580020, India",
        "city": "HUBLI-DHARWAD"
    },

    {
        "hotelName": "Omkar Rooms - Corporate Guest House",
        "hotelAddress": "Near New Bus Stand, Hubli-Dharwad 580030, India",
        "city": "HUBLI-DHARWAD"
    }

    ,



    {
        "hotelName": "Hotel Shri Ramakrishna",
        "hotelAddress": "Geethanjali Road, Udupi 576101, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Hotel Janardana",
        "hotelAddress": "Near K S R T C Bus Stand, Udupi 576101, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Paradise Lagoon",
        "hotelAddress": "Kemmannu Hoode, Udupi 576108, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Paradise Isle Beach Resort",
        "hotelAddress": "	Malpe Beach | 46, Malpe Beach, Udupi 576 108, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Samanvay Boutique Hotel",
        "hotelAddress": "Kinnimulki Main Road | Near Govinda Kalyana Mantapa, Udupi 576101, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "The Ocean Pearl",
        "hotelAddress": "Udupi - Manipal Road | Kadiyali Junction, Udupi 576102, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Tinton Homestay & Water Parks",
        "hotelAddress": "Thombathu, Hengavalli post | via Goliangadi, Kundapura Taluk, Udupi 576212, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Malpe Sea Front Cottages",
        "hotelAddress": "Malpe Beach, Udupi 576 108, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Wild Woods Spa and Resort",
        "hotelAddress": "	Kundapur Taluk | Near Mangalore, Udupi 576214, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Balkatmane Heritage Spa - A Wandertrails Stay , Beloor",
        "hotelAddress": "#314, Beloor Village | Kundapur Taluk, Udupi 576221, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Hotel Diana",
        "hotelAddress": "Near Service Bus Stand, Udupi 576101, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Hotel Udupi Residency",
        "hotelAddress": "Hubli - Dharwad Highway | Sadhunavar Hospitality, Near University of Agriculture Sciences, Hubli-Dharwad 580005, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Hotel Century Executive",
        "hotelAddress": "Sanskrith College Road | Besides Parivar Sweets, Udupi 576101, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Aryaan Resort & Residences",
        "hotelAddress": "Kadike Beach Road | Badanidiyooru, Udupi 576115, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "VITS Pratham Udupi Resort",
        "hotelAddress": "BH Road | Opp Muncipal Water Tank Kundapura, Udupi 576211, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Hotel Sri Krishna Residency",
        "hotelAddress": "Badagupete Road | near Sri Krishna Temple, Udupi 576001, India ",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Hotel Sharada International",
        "hotelAddress": "Nh 66,, Udupi 576103, India18..Treebo Vijaya Grand,Old Post Office Road | Opp Kalpapna Theater, Diana Circle, Udupi 576101, India",
        "city": "UDUPI"
    }

    ,
    {
        "hotelName": "Rukmini Residency",
        "hotelAddress": "Rukmini Arcade | Rukmini Arcade, near City Bus Stand, Udupi 576101, India",
        "city": "UDUPI"
    },

    {
        "hotelName": "Hotel Sriram Residency",
        "hotelAddress": "Head Post Office Rd | Near Krishna Temple, Opposite Head Post Office, Udupi 576101, India",
        "city": "UDUPI"
    },




    {
        "hotelName": "Hotel R K Renaissance",
        "hotelAddress": "10637 Laxmi Pride | Opp. Phoenix School J.N.M.C Road, Belgaum, India  ",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Bhimgad Adventure Camp",
        "hotelAddress": "Near K S R T C Bus Stand, Udupi 576101, India",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel Sankam Residency",
        "hotelAddress": "Gandhinagar Hotel Sankam Residency Airport Road | Gandhi Nagar, Belgaum 590016, India ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel EEFA",
        "hotelAddress": "3935 Club Road, Belgaum, India  ",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Fairfield by Marriott Belagavi",
        "hotelAddress": "Gogte Plaza, Belgaum 591113, India",
        "city": "BELGAUM"
    },

    {
        "hotelName": "UK 27 The Fern, Belagavi",
        "hotelAddress": "Civil Hospital Road Ayodhya Nagar, Belgaum 590001, India ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Pai Resort",
        "hotelAddress": "1044/3B2 Kanakdas Road | Opp. Killa Talav, Belgaum 590016, India  ",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Adarsha Palace Hotel",
        "hotelAddress": "College Road | Karnataka, Belgaum 590001, India  ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel RK Renaissance",
        "hotelAddress": "	10637 Jnmc Road | opp Phoenix School, Belgaum 590010, India  ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel Rakshit International",
        "hotelAddress": "#314, Beloor Village | Kundapur Taluk, Udupi 576221, India",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Alurkar Resorts",
        "hotelAddress": "At & Post Bugate Alur Nipani - Amboli Road | Hukkeri Taluka, Belgaum 591313, India  ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Western Lodge",
        "hotelAddress": "Kirloskar Road | below Top In Town Bar & Resturant opp Nucleus Mall, Belgaum 590001, India",
        "city": "BELGAUM"
    },

    {
        "hotelName": "India  Native by Chancery Hotels",
        "hotelAddress": "CTS No. 732, Khanapur Road | Near Tilakwadi 3rd gate, Tilakwadi, Belgaum Opp D Mart, Belgaum 590007",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "India  Hotel Surya Yatri Niwas",
        "hotelAddress": "Kapeel Tower, Belgaum 590001, India ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel Hanuman",
        "hotelAddress": "Nehru Nagar | Near JNMC, Belgaum 590010, India  ",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel Navtara",
        "hotelAddress": "Station Road, Belgaum 590001, India ",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Hotel Rajwada International",
        "hotelAddress": "Old PB Road near RTO office, Belgaum 590002, India  ",
        "city": "BELGAUM"
    }

    ,
    {
        "hotelName": "Atharv Resort",
        "hotelAddress": "Near Amboli Grampanchayat Sawantwadi Sindhudurga, Amboli 416510, India",
        "city": "BELGAUM"
    },

    {
        "hotelName": "Hotel Suvarn mandir",
        "hotelAddress": " | Khade Bazar, Belgaum 590001, India",
        "city": "BELGAUM"
    },




    {
        "hotelName": "Viva Fernleaf Resortdging in Tumkur",
        "hotelAddress": "10637 Laxmi Pride | Opp. Phoenix School J.N.M.C Road, Belgaum, India  ",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "Sri Krishna Deluxe",
        "hotelAddress": "Kiran Mansion | Opp. Govt. Hospital, B.H. Road, Tumkur, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Hotel Sri Krishna Deluxe",
        "hotelAddress": "Tumkur-Kunigal Road, Tumkur 572101, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "CNV Chambers",
        "hotelAddress": "Bh Road | Opposite Rto Office, Tumkur 572102, India  ",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "SS Residency",
        "hotelAddress": "Basaveshwara Road, behind Private Bus Stand, Tumkur 572101, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Vaishali Deluxe Comforts",
        "hotelAddress": "Opposite SP Office, B H Road, Tumkur 577101, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Vilasi Comforts",
        "hotelAddress": "Sri Shivakumara Swamy Circle, B.H. Road, Tumkur 572103, India  ",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "Aishwarya Lodge",
        "hotelAddress": "BH Road | Opp. Polytechnic College, Tumkur 572102, India ",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Sky Lodge",
        "hotelAddress": "Param Edge, B.H. Road | Next to Nayanadhama Hospital, Batawadi, Tumkur 572103, India ",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Golden Palms Hotel & Spa",
        "hotelAddress": "Golden Palms Avenue, Off Tumkur Road | Hobli, Tumkur Road, Bengaluru 562123, India",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "Hotel Raj Vista",
        "hotelAddress": "Hesaragatta Main Road | Chikkabanavara, Bengaluru 560090, India  ",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Angsana Oasis Spa & Resort",
        "hotelAddress": "Main Doddaballapur Road | Rajankunte, Northwest Country, Bengaluru 560064, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Shree Bhavani Residency",
        "hotelAddress": "shree Bhavani Residency 7 Shree Bhavani Residency 5'th Cross Road | Cottonpet, Bengaluru 560034, India",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "Alcove at Koramangala",
        "hotelAddress": "535 8'th A Main Road | Close to BDA Complex, Koramangala, Bengaluru 560034, India ",
        "city": "TUMKUR"
    },

    {
        "hotelName": "The Sambhram Roost",
        "hotelAddress": "19 Lakshmipura Cross | Hesarghatta Main Road, Via M.S.Palya, Bengaluru 560097, India  ",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Hotel Dev's Paradise",
        "hotelAddress": "#775, 7th block, 2nd phase, BSK 3rd stage Ring Road | opp Gupta College, Bengaluru 560018, India",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "The Windflower Prakruthi Bangalore",
        "hotelAddress": "Kundana Hobli Plot No 12a Hegganahalli Village | Devanahalli Taluk, Bengaluru 560052, India ",
        "city": "TUMKUR"
    }

    ,
    {
        "hotelName": "Ganga Sagar Hotel",
        "hotelAddress": "Ysr Mansion Ganga Sagar Hotel No 3, Bengaluru 560095, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Ujjwala International Hotel",
        "hotelAddress": "near Tvs Cross | Peenya Industrial Area, Bengaluru 560058, India",
        "city": "TUMKUR"
    },

    {
        "hotelName": "Treebo Rotano Suites",
        "hotelAddress": "412 353, BB Main Road, Bengaluru 560064, India",
        "city": "TUMKUR"
    },




    {
        "hotelName": "River View Farm Stay",
        "hotelAddress": "S No. 2/1 Thirthahalli Road, Shimoga, India  ",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Bright Hotel",
        "hotelAddress": "N T Road | Near New Bus Stand, Shimoga, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Padmasri Homestay",
        "hotelAddress": " | Bazar Line, Jog Falls, Shimoga Sagar, Shimoga 577401, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Devangi Atithya Home Stay",
        "hotelAddress": "Devangi Post, Thirthahalli Taluk | Malnad Region, Shimoga 577 415, India  ",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Inchara Homestay",
        "hotelAddress": "Mandagadde Post,, Shimoga 577220, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Royal Inn Thirthahalli",
        "hotelAddress": "Azad Road, Opp Corporation Bank | Thirthahalli, Shimoga 577432, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Royal Orchid Central, Shimoga",
        "hotelAddress": "B H Road, Opposite Vinayak Theatre | BH Road, Shimoga 577201, India  ",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Simha Farms",
        "hotelAddress": "Simha Farms, Kodachadri, Nittur, Hosanagar, Shimoga 566452, India ",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Hotel Mayura Gerusoppa Jogfalls",
        "hotelAddress": "Jog Falls | Sagar Taluk, Shimoga 577 435, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Kodachadri Pathalagere Homestay Or Resort",
        "hotelAddress": "Pathalagere | Kattinahole Matthikai Post, Shimoga 577452, India",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Green View Clarks Inn",
        "hotelAddress": "Balraj Urs Road | Near railway station, Shimoga 577201, India ",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Sri Ambika Homestay",
        "hotelAddress": "Sampekatte Kodachadri Road, Shimoga, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Hotel Sanman Lodge",
        "hotelAddress": "Garden Area,3rd Cross, Shimoga 577201, India",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Ashoka Lodge",
        "hotelAddress": "Agumbe Road, Opp Hdfc Bank | Thirthahalli, Shimoga 577432, India ",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Amma Residency",
        "hotelAddress": "Vinoba Nagar Amma Residency Police Chowki Circle, Shimoga 577204, India ",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Akash Inn Shimoga",
        "hotelAddress": "Rathnamma Madhav Road, Shimoga 577201, India",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Surya Comforts",
        "hotelAddress": "3rd Parallel Rd, Durgigudi, Shimoga, India ",
        "city": "SHIMOGA"
    }

    ,
    {
        "hotelName": "Mathura Residency",
        "hotelAddress": "Shimoga, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "Sri Mahalakshmi Residency",
        "hotelAddress": "Honnali Main Road | Jyothi Prakash Complex, Tavare Chanthalli, Shimoga 577201, India",
        "city": "SHIMOGA"
    },

    {
        "hotelName": "The Thash Resort",
        "hotelAddress": "B.H. Road | Thyagarthi Cross, Sagar 577401, India",
        "city": "SHIMOGA"
    },





    {
        "hotelName": "Swarga Home Stay",
        "hotelAddress": "Kumbarahalli Estate | Saklespur, Hassan 573 137, India ",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "The Karle",
        "hotelAddress": "Karle Village | Hassan Taluk, Hassan 573120, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "The Ashhok Hassan",
        "hotelAddress": " 121 P B No, Hassan 573201, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Hotel Southern Star",
        "hotelAddress": "B M Road | Opp. Big Bazaar, Hassan 573201, India  ",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Hoysala Village Resort",
        "hotelAddress": "Belur Road | Survey No. 357, Handinkere Village, Hassan 573217, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Stay Simple Riverdale",
        "hotelAddress": "Sri Ramadevara Dam Hassan 573201, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Shreevara Residency",
        "hotelAddress": "B.M. Road | Opp. Pruthvi Theatre, Hassan 573201, India ",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Hotel Raama",
        "hotelAddress": "B M Road, Opp S D M Ayurvedic Hospital & College | Thanneeruhalla, Hassan 573201, India ",
        "city": "HASSAN"
    },

    {
        "hotelName": "Hotel Mayura International",
        "hotelAddress": "Sakleshpur Road | B.M. Road, Hassan 573201, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Mallige Residency",
        "hotelAddress": "High School Field Road | K R Puram, Hassan, India",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Pavanputra Resort",
        "hotelAddress": "117, H.N Pura Road | Flat No. 114, 116, Hassan 573201, India ",
        "city": "HASSAN"
    },

    {
        "hotelName": "Aakash Hotel",
        "hotelAddress": "Sjp Road K,R, Puram Hassan, Hassan 573201, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Hotel Kadamba Comforts",
        "hotelAddress": "BM Road | Beside malabar gold, behind shubodaya kalyana mantapa, Hassan 573201, India",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Amber Castle Hotel & Suites",
        "hotelAddress": "#72 Ward No 8 | Near SBG Theatre, Hassan 573201, India ",
        "city": "HASSAN"
    },

    {
        "hotelName": "Arun Residency",
        "hotelAddress": "K Hosa Kappalu Byepass Mysore Road, Hassan 573201, India ",
        "city": "HASSAN"
    },

    {
        "hotelName": "Suvarna Regency",
        "hotelAddress": "B M Road | Bangalore - Mangalore Road, Hassan 573201, India",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Sarayu Hotel",
        "hotelAddress": "Venkatadri Towers | B M Road, Hassan, India",
        "city": "HASSAN"
    }

    ,
    {
        "hotelName": "Paramparee",
        "hotelAddress": "Merve Village, Hassan 573128, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Nature Nestate, Homestay",
        "hotelAddress": "Bangalore - Shivamogga Road No 125, Hassan 573111, India",
        "city": "HASSAN"
    },

    {
        "hotelName": "Palguni Residency",
        "hotelAddress": "R.C. Road | Opp Gandadakote, Hassan 573201, India",
        "city": "HASSAN"
    },




    {
        "hotelName": "Jungle Lodges - Bheemeshwari Nature & Adventure Camp",
        "hotelAddress": "Halagur - Muttatti Road | Bheemeshwari, Mandya, India ",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "Hotel Mayura",
        "hotelAddress": "NH-48 , Bellur Cross, Nagamandala Taluk, Mandya 571448, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Hotel Mayura Kauvery KRS",
        "hotelAddress": " Brindavan Gardens, Krishna Raja Sagara, Mandya 571607, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "KSTDC Hotel",
        "hotelAddress": "Barachukki Falls Road, Mandya 571440, India  ",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "Hotel Jyothi international",
        "hotelAddress": "Vidyaranyapura - Nanjangud Road Kirgandoor, Mandya 571401, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Hotel Surabi",
        "hotelAddress": "#D2/2225/2225 Bengaluru Mysuru Highway, Mandya 571401, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Hotel Mayura Riverview Srirangapatna",
        "hotelAddress": "Mysore Road, Srirangapatna 571 438, India",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "Young Island Resorts",
        "hotelAddress": "17/A,17/B Bommur Agrahara | Mandya District, Srirangapatna 570 005, India",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "OYO 642 The Atrium Boutique Hotel",
        "hotelAddress": "#110 Mysore Bangalore Road, Mysuru (Mysore) 570003, India ",
        "city": "MANDYA"
    },

    {
        "hotelName": "Sunflower Hotel",
        "hotelAddress": "331 Mysore Bangalore Road | Bannimantap, Mysuru (Mysore) 570015, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "FabHotel Dawn Mysore",
        "hotelAddress": "15 Mysore Bangalore Road | Mysore Bangalore Rd, Bannimantap,, Mysuru (Mysore) 570015, India",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "OYO 11535 Sri Sai Comforts",
        "hotelAddress": "122/A, 3rd Main Road, Bannimantap, Mysuru (Mysore) 570015, India ",
        "city": "MANDYA"
    },

    {
        "hotelName": "Lalitha Mahal Palace Hotel",
        "hotelAddress": "Lalitha Mahal Road, Mysuru (Mysore) 570028, India ",
        "city": "MANDYA"
    },

    {
        "hotelName": "Regalia Inn & Suites",
        "hotelAddress": "#3845/1, Karunapura Road | Near FTS Circle, Mysuru (Mysore) 570007, India",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "Treebo Akshaya Mahal Inn",
        "hotelAddress": "A No 5 Abba Road Hydarali Road | Nazarabad Mohalla, Mysore, Mysuru (Mysore) 570007, India",
        "city": "MANDYA"
    }

    ,
    {
        "hotelName": "Fortune JP Palace",
        "hotelAddress": "3, Abba Road Nazarbad, Mysuru (Mysore) 570007, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "OYO 9133 The Olive Star",
        "hotelAddress": "Dr. Nelson Mandela Road | New Bannimantap Extension, Mysuru (Mysore) 570015, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Villa Park",
        "hotelAddress": "#25, Vasantha Mahal Road, | Opp. Chamundi Vihar Stadium Nazarbad, Mysuru (Mysore) 570011, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Leela Residency",
        "hotelAddress": "351 Leela Residency Ashoka Road | Near St. Philominia's Church, Mysuru (Mysore) 570001, India",
        "city": "MANDYA"
    },

    {
        "hotelName": "Hotel Sunshine",
        "hotelAddress": "#354 Ashoka Road | Near St. Philomena's Church, Ashoka Road, Mysuru (Mysore) 570001, India",
        "city": "MANDYA"
    },




    {
        "hotelName": "Hotel Golden Regency",
        "hotelAddress": "Station Road, Behind Ak Asian Grand, Gulbarga 585102, India ",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Hotel Meridian Inn",
        "hotelAddress": "8-1305/66/B, Humnabad Highway | Kapnoor Area, Gulbarga 585104, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Lumbini's Grand Hotel",
        "hotelAddress": " Near Rang Mandir Public Garden | Opp Dist Balabhavan, Gulbarga 585105, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "The Central Park Hotel",
        "hotelAddress": "Opp. Gescom,Station Road, Gulbarga 585102, India  ",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Sangam Residency Luxury",
        "hotelAddress": "Sangam Residency Luxury Nagchoon Road Plot No 1 - 9b | Khuba Plot Station Bazzar, Gulbarga 585102, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Atharva Hotel",
        "hotelAddress": "Ram Mandir Circel.High Court Rood-Gulbarga, Gulbarga 585105, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Hotel City Park",
        "hotelAddress": "Timmapuri Circle | Station Road, Gulbarga 585102, India",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Hotel Ashraya Comforts Gulbarga",
        "hotelAddress": "Kadechur Central, opp City Municipal Corporation, Main Road | Jagat Circle, Gulbarga, India",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Hotel Mathura",
        "hotelAddress": "Station Main Road | Kandoor Mall, 3rd floor, S V P Circle, Gulbarga 585102, India ",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Hotel Pariwar",
        "hotelAddress": "No 1 - 73/1 Station Road | Opposite To Kptcl, Gulbarga 585102, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "SLV Lodging & Boarding",
        "hotelAddress": "Chittapur Road | near Subash Chowk, Yadgir 585201, India",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Hotel Kadamba Residency",
        "hotelAddress": "Dr Jawali Complex, Super Market, Gulbarga, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Udyan Residency",
        "hotelAddress": "Garden Road | Behind Center Kamat, Gulbarga, India ",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Sun International Hotel",
        "hotelAddress": "72,Jewargi Road, Near Bhagyawanti Nagar, Gulbarga, Karnataka, Gulbarga, India",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Al-Bahlool Guest House",
        "hotelAddress": "Bada Roza, Dargah Road, Gulbarga 585104, India",
        "city": "GULBARGA"
    }

    ,
    {
        "hotelName": "Panchami International Hotel",
        "hotelAddress": "Dr Jawali Complex | Supermarket, Gulbarga, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Sai Datta Lodge",
        "hotelAddress": "Main Road Ganagapur, Gulbarga 585212, India",
        "city": "GULBARGA"
    },

    {
        "hotelName": "Sri Amba Bhavani Hotel",
        "hotelAddress": "2nd Floor, Fort Road | Near Prakash Takies, Gulbarga, India",
        "city": "GULBARGA"
    },




    {
        "hotelName": "Amogha International Hotel",
        "hotelAddress": "Santhe Honda Road, Chitradurga 577 501, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Hotel Ravi Mayur International",
        "hotelAddress": "NH-4, opposite police Samudhaya Bhavan, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "KSTDC Hotel Mayura Yatrinivas Chitradurga",
        "hotelAddress": "Next to Maharani College, opposite to Fort, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Hotel Naveen Regency",
        "hotelAddress": "7/3 B NH-4 Bypass, Chitradurga 577501, India  ",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "V.G.S Comforts",
        "hotelAddress": "Opp.KSRTC Bus Stand, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Jagular Mahalingappa Comforts",
        "hotelAddress": "B D Road, Near To Government Bus Stand, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Mourya Deluxe A/C Lodge",
        "hotelAddress": "Santhe Bagilu Road | Near Ane Bagilu, Chitradurga 577 501, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Hotel G.B.T. Comforts",
        "hotelAddress": "NH-4 & NH-13 Circle, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Mango Hotels Naveen",
        "hotelAddress": "| BSC Exclusive, beside KFC, MCC - B Block, opp Bapuji Dental Hospital, Davangere 577002, India ",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Srigandha Residency",
        "hotelAddress": "PB Road | Vinayaka Complex, Near Aruna theatre, Davangere 577002, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Sankama The Boutique Hotel",
        "hotelAddress": "Shamanur Road, Davangere 577004, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Apoorva Resorts",
        "hotelAddress": "near Bada Cross, N. H. 4, Davangere, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Genesis Retreat Resort",
        "hotelAddress": "3621 Kundwada village, NH-4, Bypass Road, Davangere 577004, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Southern Star Hotel & Convention",
        "hotelAddress": "Shamanur Road | Next to Lakshmi Flour mills, Davangere 577004, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Sai Residency",
        "hotelAddress": "P.B. Road, Davanagere 577002, India",
        "city": "CHITRADURGA"
    }

    ,
    {
        "hotelName": "Hotel Pooja International",
        "hotelAddress": "Pune-Bangalore Road, Davangere 577006, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Hotel Shoven",
        "hotelAddress": "Harihar Road | KINI Towers, Near DC Office, Near Karur Industrial Area, Davangere 577006, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Hotel Shanthi Park",
        "hotelAddress": "216 Ashoka Road | P.B Road, Davangere 577002, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Sai International Hotel",
        "hotelAddress": "No 18, 19, 20 Harihara Road, Davangere 577006, India",
        "city": "CHITRADURGA"
    },

    {
        "hotelName": "Hotel Durgada Siri",
        "hotelAddress": " | Near RTO Office, Chitradurga 577501, India",
        "city": "CHITRADURGA"
    },




    {
        "hotelName": "IROOMZ Vaishali Residency",
        "hotelAddress": "94/16,TS No 97, Bellary 583101, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Hotel Allum",
        "hotelAddress": "Bellary Ring Rd | Beside Allum Bhavan, Near Auto Nagar Check Post, Bellary 583101, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "IRoomz Dwaraka Residency",
        "hotelAddress": "Hiriyur Road near New KSRTC Bus Stand Bapu Ji Nagar, Bellary 577599, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Hotel Nakshatra-LR",
        "hotelAddress": "opp Anjana Diagnostic Center, Gopal Swamy Road, Gandhi Nagar, Bellary, India  ",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Hotel Rock Regency",
        "hotelAddress": "Shankaragudda Colony | JSW Steel Ltd., Toranagallu , Close to Hampi, Bellary 583123, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Pola Paradise",
        "hotelAddress": "92/A New Tank Bund Road, Bellary 583104, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Hotel Mayura Vijayanagara TB Dam",
        "hotelAddress": "TB Dam, Hospet, Bellary 583 225, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Marchad Residency",
        "hotelAddress": "No.102, Ambedkar Complex | 1ST Floor, Beside Bus Stand, Bellary, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Hotel Bala Regency",
        "hotelAddress": "16/13 Parvathi Nagar Main Road | Near Hampi, Bellary 583103, India ",
        "city": "BELLARY"
    },

    {
        "hotelName": "Hotel Royal Fort",
        "hotelAddress": "New Trunk Road | behind Hotel Pola Paradise, Bellary 583104, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "IROOMZ Hotel Pawan",
        "hotelAddress": "140/189, Anantpur Road, Bellary 583101, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Grande Idhanta",
        "hotelAddress": "Dr Rajkumar Road, Off Royal Circle, Bellary 583101, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Hotel Ashoka Comforts",
        "hotelAddress": "115 Court Road | Court Road, Bellary, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Manasa Inn",
        "hotelAddress": "Dr. Rajkumar Road Bellary, Bellary 583101, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Evolve Back, Hampi",
        "hotelAddress": "| Orange County, Hampi Hallikeri Village, Kamlapura Post, Kamalapur 583221, India",
        "city": "BELLARY"
    }

    ,
    {
        "hotelName": "Hotel Mayura Bhuvaneshwari Kamalapur",
        "hotelAddress": "Hospet | Bellary District, Kamalapur, Hampi 583 221, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Clarks Inn Hampi",
        "hotelAddress": "HPC Road | Opp A.S.I Museum Kamalapur ,Hospet Taluk, Karnataka, Hampi 583221, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Shivavilas Palace, HAMPI (An ITC WelcomHeritage Hotel)",
        "hotelAddress": "No 97 Shivavilas Palace Hotel Palace Road | Hampi, Sandur 583239, India",
        "city": "BELLARY"
    },

    {
        "hotelName": "Heritage Resort Hampi",
        "hotelAddress": "48/C & 4b Hosamalapanagudi State Highway 49, Hampi 583239, India",
        "city": "BELLARY"
    },




    {
        "hotelName": "Mango Hotels Naveen",
        "hotelAddress": "| BSC Exclusive, beside KFC, MCC - B Block, opp Bapuji Dental Hospital, Davangere 577002, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Sai International Hotel",
        "hotelAddress": "No 18, 19, 20 Harihara Road, Davangere 577006, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Srigandha Residency",
        "hotelAddress": "PB Road | Vinayaka Complex, Near Aruna theatre, Davangere 577002, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Apoorva Resorts",
        "hotelAddress": "near Bada Cross, N. H. 4, Davangere, India  ",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Hotel Pooja International",
        "hotelAddress": "Pune-Bangalore Road, Davangere 577006, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Sankama The Boutique Hotel",
        "hotelAddress": "Shamanur Road, Davangere 577004, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Hotel Shoven",
        "hotelAddress": "Harihar Road | KINI Towers, Near DC Office, Near Karur Industrial Area, Davangere 577006, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Hotel Shanthi Park",
        "hotelAddress": "216 Ashoka Road | P.B Road, Davangere 577002, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Southern Star Hotel & Convention",
        "hotelAddress": "Shamanur Road | Next to Lakshmi Flour mills, Davangere 577004, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Hotel Sri Nandi",
        "hotelAddress": "Near Bus Stand, Ranebennur 581115, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Amogha International Hotel",
        "hotelAddress": "Santhe Honda Road, Chitradurga 577 501, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Green View Clarks Inn",
        "hotelAddress": "Balraj Urs Road | Near railway station, Shimoga 577201, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Surya Comforts",
        "hotelAddress": "3rd Parallel Rd, Durgigudi, Shimoga, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Hotel Jai Maata Grandeu",
        "hotelAddress": "12 Vidya Nagar Main Road, BH Road, Shimoga 577201, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Hotel Jewel Rock",
        "hotelAddress": "Durgigudi Road, Shimoga, India",
        "city": "DAVANGERE"
    }

    ,
    {
        "hotelName": "Royal Orchid Central, Shimoga",
        "hotelAddress": "B H Road, Opposite Vinayak Theatre | BH Road, Shimoga 577201, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Sai Residency",
        "hotelAddress": "P.B. Road, Davanagere 577002, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Jagular Mahalingappa Comforts",
        "hotelAddress": "B D Road, Near To Government Bus Stand, Chitradurga 577501, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "KSTDC Hotel Mayura Yatrinivas Chitradurga",
        "hotelAddress": "Next to Maharani College, opposite to Fort, Chitradurga 577501, India",
        "city": "DAVANGERE"
    },

    {
        "hotelName": "Mourya Deluxe A/C Lodge",
        "hotelAddress": "Santhe Bagilu Road | Near Ane Bagilu, Chitradurga 577 501, India",
        "city": "DAVANGERE"
    },





    {
        "hotelName": "Clarks Inn Bagalkot",
        "hotelAddress": "Badami Flyover Road | Behind Railway Station, Bagalkot 587101, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "Hotel Hari Priya Bagalkot",
        "hotelAddress": "Apmc, Bagalkot 587101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Kanthi Resorts",
        "hotelAddress": "Sy No 109 , Gaddankeri | Nh , 218, Bagalkot, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Shiva Sangam Residency",
        "hotelAddress": "Kanthi Nagar | Near A.P.M.C. Navanagar, Bagalkot, India ",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "Parag Boarding & Lodging",
        "hotelAddress": "Gaddanakeri cross, Bagalkot 587102, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Krupa Lodge",
        "hotelAddress": "Opp Bus Stand, Near to Railway Station, Bagalkot 587101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Akshay International Hotel",
        "hotelAddress": "SH 57, Sector 22, Bagalkot, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "Clarks Inn Badami",
        "hotelAddress": "Badami Main Road, 1755 Veerpulakeshi Circle, Badami 587201, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "The Heritage Resort",
        "hotelAddress": "Station Road | A Unit of Masur Garden Resorts Pvt Ltd, Badami 587201, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Hotel Anand Deluxe",
        "hotelAddress": "Station Road, Near Bus Stand, Badami 587201, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "I Roomz Sanman Deluxe",
        "hotelAddress": "Station road | Near bus stand, Badami 587201, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "Krishna Heritage",
        "hotelAddress": "Ramdurg Road, Badami, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Hotel Mayura Chalukya Badami",
        "hotelAddress": "Badami, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Hotel Megharaj",
        "hotelAddress": "Railway Station Road | Opp Bank of India, Bijapur 586101, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "Shivaratna Palace",
        "hotelAddress": "Pala Badami Road, Gadag 582102, India",
        "city": "BAGALKOT"
    }

    ,
    {
        "hotelName": "VKG Complex Hotel Sri Udupi Park Lodging & Boarding",
        "hotelAddress": "Bhasaveshwar Circle | Near Jayshree Talkies, Bijapur 586101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Atithi Comfort Lodge",
        "hotelAddress": "M.G. Road, Gandhi Chowk | Horti Tower, Bijapur 586101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Hotel Pancharatan Delux",
        "hotelAddress": "Ratan Mall, SS Road, Bijapur 586101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Sarvodaya Deluxe Lodge",
        "hotelAddress": "VVs College Road | Opposite Bus Stop, Bagalkot 587101, India",
        "city": "BAGALKOT"
    },

    {
        "hotelName": "Anand Deluxe Hotel",
        "hotelAddress": "Badami | Near Bus Stand, Bagalkot, India",
        "city": "BAGALKOT"
    },




    {
        "hotelName": "Sai Palace Lodge",
        "hotelAddress": "| Opp New Bus Stand, Behind Mayura Hotel, Bidar 585401, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Blackbuck Resort",
        "hotelAddress": "Vilaspur tank, Bidar 585401, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Shiva International",
        "hotelAddress": "Opp. Akkamahadevi College, Beside Reliance Petrol Bunk, New Bus Stand Road, Bidar 585 401, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Hotel Sapna International",
        "hotelAddress": "Udgir Road, Bidar 585401, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Vikas Residency Hotel",
        "hotelAddress": "Mohan Market | Railway Station Bidar, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Kailash Hotel",
        "hotelAddress": "Mahipal Singh Pawar Complex | Near Old Bus Stand, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Hotel Mahindra Paradise",
        "hotelAddress": "Nh-9 | Huggeli Village, Zaheerabad 502220, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Mayura Barid Shahi Hotel",
        "hotelAddress": "Udgir Road | Near Ambedkar Circle, Bidar, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Hotel Mayura",
        "hotelAddress": "Opposite New Bus Stand, Bidar 585401, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Hotel Shanti",
        "hotelAddress": "Opp. Indira Market, Near Old Bus Stand, Bidar 585401, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Ashoka Hotel",
        "hotelAddress": "Near Deepak Film Talkies | Opposite Sales TAX Office, Bidar, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Basava Bhavan Hotel",
        "hotelAddress": "Own, Main Raod Hulsoor | Near Police Station, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Hotel Ashoka Bidar",
        "hotelAddress": "| Near deepak theater, Bidar 585401, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Hotel Deccan",
        "hotelAddress": "Mughle Complex | Opposite To Shapur Masjid, Bidar, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Kaveri Hotel",
        "hotelAddress": "Jabshetty Market | Near Nandi Petrol Bunk, Bidar, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Khasim Biryani Hotel",
        "hotelAddress": "Beside Zoom Computer | Opp TPS Bhalki, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Mohammed Hotel",
        "hotelAddress": "Humnabad | Near Manik Nagar Kaman, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Paradise Hotel",
        "hotelAddress": "Basveshwar Circle | Naubad, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Prince Hotel",
        "hotelAddress": "8-9-108, Zyd Nanded Road | Udgir Road, Bidar, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Around the Village",
        "hotelAddress": "At Post Village Nittur | TQ Bhalki, Bidar, India",
        "city": "BIDAR"
    },




    {
        "hotelName": "Hotel Gets Grand",
        "hotelAddress": "No. 310, Jodi Krishnapura | Narasapura Industrial Area, Kolar 563133, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Signature Club Resort",
        "hotelAddress": "NH 207, Budigere Road, Brigade Orchards | Rayasandra Village, Devanahalli, Bengaluru 562110, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "OYO 9419 Manu Residency",
        "hotelAddress": "Hanuman Road | Sai colony, Kadugodi, Whitefield flyover, Bengaluru 560067, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Four Points by Sheraton Bengaluru, Whitefield",
        "hotelAddress": "43/3 White Field Main Road, Bengaluru 560067, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Hotel Royal Orchid Suites, Whitefield, Bangalore",
        "hotelAddress": "Vaswani Pinnacle Annexe | Whitefield Main Road, Bengaluru 560066, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Vivanta By Taj Whitefield",
        "hotelAddress": "ITPB, Whitefield, Bengaluru 560066, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "The Den",
        "hotelAddress": "ITPL Main Road, Whitefield | Bangalore, Bengaluru 560066, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Royal Residency",
        "hotelAddress": "Old Madras Road | Opp RTO Office, Medahalli, Bengaluru 560049, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Oakwood Residence Whitefield Bangalore",
        "hotelAddress": "#62, Forum Value Mall, | Whitefield Main Road, Whitefield, Bengaluru 560066, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Taj Bangalore",
        "hotelAddress": "Near Kempegowda International Airport, Devanhalli 560300, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Aloft Bengaluru Whitefield",
        "hotelAddress": "17c Sadaramangala Road | Off Whitefield Main Road, Opposite ITPL, Bengaluru 560066, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Sheraton Grand Bengaluru Whitefield Hotel & Convention Center",
        "hotelAddress": "Prestige Shantiniketan | Hoodi Whitefield, Bengaluru 560048, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Crest Executive Suites",
        "hotelAddress": "1 Prestige Shantiniketan Gate No | ITPL Main Road, Whitefield, Bengaluru 560048, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Bulande Comforts",
        "hotelAddress": "ITPL Main Road, B | Opp PSN Near ITPL Whitefield, Bengaluru 560066, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Chris Hotel Whitefield",
        "hotelAddress": "120 - A3 Santosh Towers Epip Zone | Near KTPO, ITPL Main Road No-2, Whitefield, Bengaluru 560066, India",
        "city": "BIDAR"
    }

    ,
    {
        "hotelName": "Days Suites Bengaluru Whitefield",
        "hotelAddress": "SY. NO. 139, Rajpalya,Hoodi Village,ITPL Main Road | Opp Prestige Shantineketan Whitefield, 560048, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Vijay Residency ( formerly Comfort INN)",
        "hotelAddress": "N. 18, III Main Road | Gandhinagar, Bengaluru 560009, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Bengaluru Marriott Hotel Whitefield",
        "hotelAddress": "75 8th Road | Plot No 75, EPIP Area, Whitefield, Bengaluru 560066, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "The Zuri Whitefield Bangalore",
        "hotelAddress": "ITPL Road | 244 Hoody Village, Rajapalya, Whitefield, Bengaluru 560048, India",
        "city": "BIDAR"
    },

    {
        "hotelName": "Evoma",
        "hotelAddress": "#14 Old Madras Road | Near Pashmina Waterfront, KR Puram, Bengaluru 560049, India",
        "city": "BIDAR"
    },





    {
        "hotelName": "Rest Inn Guest House",
        "hotelAddress": "Upstairs KK Sports, Jail Road, near DDPI office, Raichur 584101, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "SLV Lodging & Boarding",
        "hotelAddress": "Chittapur Road | near Subash Chowk, Yadgir 585201, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Vinay Residency",
        "hotelAddress": "Gangavathi Road, | Near Sri Satya Garden, Sindhanur 584124, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Sreeniketanam Lodge",
        "hotelAddress": "| Beside Sri Raghavendra Swamy Temple, Mantralayam 518345, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Sri Upendra Theertha Nilayam",
        "hotelAddress": "Near State Bank of India Manthralayam, Mantralayam 518345, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Bheemas Regency",
        "hotelAddress": "20/29 P Vbs Road, Adoni 518301, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Slv Tourist Lodge",
        "hotelAddress": "Station Road, Raichur, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Bhramramba Residency, Maski",
        "hotelAddress": "Lingasuguru Road | opp Suryanagar, Raichur 584124, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Banashakari Hotel",
        "hotelAddress": "Kankadas Circal | Maski Raichur, Raichur, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Bhramramba Residency",
        "hotelAddress": "Surya Nagar, Lingasuguru Road | Maski, Raichur 584124, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Brindavana Lodge",
        "hotelAddress": "Jail Road, Raichur 584101, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Hotel Vasanth Mahal",
        "hotelAddress": " | Opp. NEKTC Bus Stand, Sindhanur, Raichur 584101, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Mohiuddin Residency",
        "hotelAddress": "Bus Station Road, Raichur 584101, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Taj Comforts Stay Deluxe",
        "hotelAddress": "KSCM Complex ,Kustagi Road ,Sindhanur, Raichur, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Hotel Nrupatunga",
        "hotelAddress": "Station Road, Raichur, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Geetha Lodge Deluxe",
        "hotelAddress": "Opp Bus Stand, Sindhanur, Raichur 584101, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Hotel Santoshi Sarovar",
        "hotelAddress": "Sation Road | Opp Public Garden, Raichur 584101, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Raj-Inn Comfort",
        "hotelAddress": "near EK Minar Masid Navrang Darwaza Road, Raichur 584101, India",
        "city": "RAICHUR"
    },

    {
        "hotelName": "Hotel Sri Lakshmi",
        "hotelAddress": "Bus Station Road, Raichur 584101, India",
        "city": "RAICHUR"
    },



    {
        "hotelName": "Hotel Mourya",
        "hotelAddress": "Mundargi Road, Near New Bus Stand, Gadag 582101, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "Keshav Clarks Inn",
        "hotelAddress": "Mulgund Naka Hubli Road, Gadag 582103, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Hotel Shivani Inn",
        "hotelAddress": "Mundargi Road | Near New Bus Stand, Gadag, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Shivaratna Palace",
        "hotelAddress": "Pala Badami Road, Gadag 582102, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "Hotel Swarnaa Paradise",
        "hotelAddress": "2 Station Road | Near Railway Station, Hubli-Dharwad 580020, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Samrat Ashok Hotel",
        "hotelAddress": "Lamington Road, Hubli-Dharwad 580020, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Hotel Metropolis",
        "hotelAddress": "Koppikar Road, Hubli-Dharwad 580020, India",
        "city": "RAICHUR"
    }

    ,
    {
        "hotelName": "Clarks Inn Badami",
        "hotelAddress": "Badami Main Road, 1755 Veerpulakeshi Circle, Badami 587201, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "Clarks Inn Airport Hotel",
        "hotelAddress": "Gokul Road, Hubli-Dharwad 580030, India",
        "city": "GADAG"
    },

    {
        "hotelName": "SKD Comforts",
        "hotelAddress": "Hubli-Dharwad Road | 3rd Floor, SKD Heights, Hubli-Dharwad 580021, India",
        "city": "GADAG"
    },

    {
        "hotelName": "The Hans Hotel",
        "hotelAddress": "Vidyanagar P B Road, Hubli-Dharwad 580031, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "The President Hotel, Hubli",
        "hotelAddress": " #194/1A, Opp. Unakal Lake | Srinagara Cross, P.B. Road, Hubli-Dharwad 580 031, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Hotel Naveen",
        "hotelAddress": " | Unkal Lake, Hubli-Dharwad 580025, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Denissons Hotel",
        "hotelAddress": "110 Dollors Colony, Airport Road, Hubli-Dharwad 580030, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "Mayur Aaditya Resort",
        "hotelAddress": "P B Road | Dharwad, Hubli-Dharwad 580009, India",
        "city": "GADAG"
    }

    ,
    {
        "hotelName": "Ramya Residency",
        "hotelAddress": "Pune Bangalore Bypass, Hubli-Dharwad 580001, India",
        "city": "GADAG"
    },

    {
        "hotelName": "The Verda Dwarawata",
        "hotelAddress": "Hubli - Dharwad Highway | Sadhunavar Hospitality, Near University of Agriculture Sciences, Hubli-Dharwad 580005, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Hotel Mayura Chalukya Badami",
        "hotelAddress": "Badami, India",
        "city": "GADAG"
    },

    {
        "hotelName": "Hotel Dhammangi Comforts",
        "hotelAddress": "P.B. Road | Opp to Old KSRTC Bus-Stand, Hubli-Dharwad 580029, India",
        "city": "GADAG"
    },




    {
        "hotelName": "Hotel Shivashakti Palace",
        "hotelAddress": "Old P. B. Road, opp. ICICI Bank, Ashwini Nagar, Haveri 581110, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Kadlis Hotel Ashoka",
        "hotelAddress": "P B Road Haveri Near LIC Office, Haveri 581110, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Hotel Sri Nandi",
        "hotelAddress": "Near Bus Stand, Ranebennur 581115, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "The Wild Club & Resorts",
        "hotelAddress": "Shinganahalli 581346, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Srigandha Residency",
        "hotelAddress": "PB Road | Vinayaka Complex, Near Aruna theatre, Davangere 577002, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Sankama The Boutique Hotel",
        "hotelAddress": "Shamanur Road, Davangere 577004, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Mango Hotels Naveen",
        "hotelAddress": "| BSC Exclusive, beside KFC, MCC - B Block, opp Bapuji Dental Hospital, Davangere 577002, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Hotel Metropolis",
        "hotelAddress": "Koppikar Road, Hubli-Dharwad 580020, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Hotel Swarnaa Paradise",
        "hotelAddress": "2 Station Road | Near Railway Station, Hubli-Dharwad 580020, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Samrat Ashok Hotel",
        "hotelAddress": "Lamington Road, Hubli-Dharwad 580020, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Clarks Inn Airport Hotel",
        "hotelAddress": "Gokul Road, Hubli-Dharwad 580030, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Denissons Hotel",
        "hotelAddress": "110 Dollors Colony, Airport Road, Hubli-Dharwad 580030, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "SKD Comforts",
        "hotelAddress": " Hubli-Dharwad Road | 3rd Floor, SKD Heights, Hubli-Dharwad 580021, India",
        "city": "HAVERI"
    },
    {
        "hotelName": "The President Hotel, Hubli",
        "hotelAddress": "#194/1A, Opp. Unakal Lake | Srinagara Cross, P.B. Road, Hubli-Dharwad 580 031, India",
        "city": "HAVERI"
    }

    ,
    {
        "hotelName": "Ramya Residency",
        "hotelAddress": "Pune Bangalore Bypass, Hubli-Dharwad 580001, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Hotel the CULT",
        "hotelAddress": "Varur Hubli NH 4 Poona Bengalooru Highway, Hubli-Dharwad 581207, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Hotel Shoven",
        "hotelAddress": "Harihar Road | KINI Towers, Near DC Office, Near Karur Industrial Area, Davangere 577006, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Areca Valley Stay",
        "hotelAddress": "Hulgol, Sirsi 581402, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Hotel Pooja International",
        "hotelAddress": "Pune-Bangalore Road, Davangere 577006, India",
        "city": "HAVERI"
    },

    {
        "hotelName": "Keshav Clarks Inn",
        "hotelAddress": "Mulgund Naka Hubli Road, Gadag 582103, India",
        "city": "HAVERI"
    },





    {
        "hotelName": "Peenya Gymkhana Residency",
        "hotelAddress": "3rd Main Road Plot No. 358, Chikkaballapur 560058, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "Mount View Resort",
        "hotelAddress": "Karahalli Cross | Nandi Hills Road, Devanhalli, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Hotel Mount Palazzo",
        "hotelAddress": "42/1, Karahalli Cross | Devanahalli Taluk, Bengaluru 562110, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Taj Bangalore",
        "hotelAddress": "Near Kempegowda International Airport, Devanhalli 560300, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "Jade735 - Retreat By Design",
        "hotelAddress": "735 Jade Garden Phase Ii | International Airport Rd, Devanhalli 562110, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Clarks Exotica Convention Resort & Spa",
        "hotelAddress": "Swiss Town, Hollywood Junction, Sadahalli Post, Devanahalli Road, Bengaluru 562 110, India",
        "city": "CHIKKABALLAPUR"
    },
    {
        "hotelName": "Airport Residency Bangalore",
        "hotelAddress": "GKVK Layout, Yerthiganahalli | Bangalore International Airport area, Bengaluru 560068, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "Shivas Gateway",
        "hotelAddress": "Bangalore Int'l Airport Main Road | Down Town Park Siddhagiri Village Near Sadahalli Gate, Bengaluru 562157, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "Regenta Inn Airport Road Bangalore",
        "hotelAddress": "No.15, Down Town Park, Sadahalli Road & Gate | Bangalore International Airport Road, Next to ITC Factory Karnataka, Bengaluru 562157, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Fantasy Golf Resort",
        "hotelAddress": "New Airport Road | Opp. ITC Factory , NH7, Bengaluru 562157, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "The Windflower Prakruthi Bangalore",
        "hotelAddress": "Kundana Hobli Plot No 12a Hegganahalli Village | Devanahalli Taluk, Bengaluru 560052, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "Breeze Suites",
        "hotelAddress": "Brindavan Layout, near Decathlon, opp Reliance Petrol Pump | New International Airport Road, Chikkajala, Bengaluru 562157, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Arra Grande Suites",
        "hotelAddress": " No. 33, Emerald Nest, Prestige Enclave, Kempegowda International Airport Road | Next to Sha Shib Aviation & Engineering Collage, Vidyanagara Cross, Off NH-7, Yelahanka 56215",
        "city": "CHIKKABALLAPUR"
    },
    {
        "hotelName": "Angsana Oasis Spa & Resort",
        "hotelAddress": "Main Doddaballapur Road | Rajankunte, Northwest Country, Bengaluru 560064, India",
        "city": "CHIKKABALLAPUR"
    }

    ,
    {
        "hotelName": "OYO 2605 Adyar Ananda Bhavan Hotel",
        "hotelAddress": "Plot No. 490, Sonnappanahalli, Bettahalsur Post, V. I. T. Cross, New Airport Road, 562157, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "OYO 10822 Hotel Airport Comfort",
        "hotelAddress": "81 Hunsemarenhalli Airport Road | Next To Viva Toyota Showroom, Bengaluru 560075, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Jain Farms",
        "hotelAddress": "Bagalur-Malur Road, Bengaluru 560004, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "Golds Club & Resort",
        "hotelAddress": "Goldline Airciti, Kadusonapanahalli, Kannur, Bengaluru 562149, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "OYO 9653 Ample Premium Suites",
        "hotelAddress": "Vinayaka Public School Road | #1&2, Vinayaka Nagar, 3rd Main, Bagaluru Cross, IAF Post, Near Cauvery Nilaya, Bengaluru 560063, India",
        "city": "CHIKKABALLAPUR"
    },

    {
        "hotelName": "The Sai Leela Hotel",
        "hotelAddress": "No.3/1, Sai Leela Singanayakanahalli | Yelahanka, Bengaluru 560064, India",
        "city": "CHIKKABALLAPUR"
    },








    {
        "hotelName": "Hotel Samartha Comforts",
        "hotelAddress": "Malleshwara complex | Gangavathi, Koppal, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Manik Residency",
        "hotelAddress": "Jawahar Road | Banikatti, Koppal 583231, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Hotel Shivananda",
        "hotelAddress": "Collage Road | Behind K S R T C Bus Station, Hospet 583201, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Royal Orchid Central Kireeti",
        "hotelAddress": "Station Road, Hospet 583239, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Hotel Priyadarshini Pride",
        "hotelAddress": "Station Road | 205/1, Hospet 583201, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Hotel Priyadarshini Classic",
        "hotelAddress": "No 5/45 A Station Road, Hospet 583201, India",
        "city": "KOPPAL"
    },
    {
        "hotelName": "Shanbhag Towers International",
        "hotelAddress": "Shanbhag Circle | College Road, Hospet 583201, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Hotel Krishna Palace",
        "hotelAddress": "Station Road, Hospet 583201, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Hotel Malligi",
        "hotelAddress": "10/90 J.N. Road | Hampi, Hospet 583201, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Vijayshree Resort & Heritage Village,Hampi",
        "hotelAddress": "V.R. Bhurat Nagari | Hampi Road, Malpangudi 583239, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Heritage Resort Hampi",
        "hotelAddress": "48/C & 4b Hosamalapanagudi State Highway 49, Hampi 583239, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Kishkinda Heritage Resort",
        "hotelAddress": "Near Stone Bridge Cross | Sanapur, Anegondi, Hampi 583234, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Shivavilas Palace, HAMPI (An ITC WelcomHeritage Hotel)",
        "hotelAddress": " No 97 Shivavilas Palace Hotel Palace Road | Hampi, Sandur 583239, India",
        "city": "KOPPAL"
    },
    {
        "hotelName": "Clarks Inn Hampi",
        "hotelAddress": "HPC Road | Opp A.S.I Museum Kamalapur ,Hospet Taluk, Karnataka, Hampi 583221, India",
        "city": "KOPPAL"
    }

    ,
    {
        "hotelName": "Hotel Mayura Bhuvaneshwari Kamalapur",
        "hotelAddress": "Hospet | Bellary District, Kamalapur, Hampi 583 221, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Evolve Back, Hampi",
        "hotelAddress": " | Orange County, Hampi Hallikeri Village, Kamlapura Post, Kamalapur 583221, Indiaa",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Hyatt Place Hampi",
        "hotelAddress": "Vidyanagar Township | Toranagallu, Bellary 583123, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Hotel Rock Regency",
        "hotelAddress": "Shankaragudda Colony | JSW Steel Ltd., Toranagallu , Close to Hampi, Bellary 583123, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Keshav Clarks Inn",
        "hotelAddress": "Mulgund Naka Hubli Road, Gadag 582103, India",
        "city": "KOPPAL"
    },

    {
        "hotelName": "Hotel Mayura Vijayanagara TB Dam",
        "hotelAddress": "TB Dam, Hospet, Bellary 583 225, India",
        "city": "KOPPAL"
    },




    {
        "hotelName": "Hotel Kamat Plus",
        "hotelAddress": "Dr Dinakar Desai Road, Ankola 581314, India",
        "city": "UTTARKANNADA"
    }

    ,
    {
        "hotelName": "Dandeli Jungle Masti",
        "hotelAddress": "Haliyal Road, Dandeli 581325, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Vihar Homestay",
        "hotelAddress": "Hostota | Siddapura Taluk, Uttara Kannada District, Sirsi 581331, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Jungle Hillmist Stay",
        "hotelAddress": "Dandeli 581325, India",
        "city": "UTTARKANNADA"
    }

    ,
    {
        "hotelName": "River Valley Farmstay",
        "hotelAddress": "Rao Sahib Villa, Dandeli 581325, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Yana Stay",
        "hotelAddress": "NS Hegde, Kumta 581343, India",
        "city": "UTTARKANNADA"
    },
    {
        "hotelName": "Sea View Resort",
        "hotelAddress": "Church Beach Road, Murdeshwar 403516, India",
        "city": "UTTARKANNADA"
    }

    ,
    {
        "hotelName": "Jungle Century Resort",
        "hotelAddress": "Dandeli, Dandeli, India",
        "city": "UTTARKANNADA"
    }

    ,
    {
        "hotelName": "Pragati Home Stay",
        "hotelAddress": "Nagarakodi, Shamemane | Via Tangarmane, PO Balesara, Sirsi 581450, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Bison River Resort",
        "hotelAddress": "Village - Ilva | Post District - Uttar Kannada, Ganeshgudi 581365, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Dandeli Dreams Homestay",
        "hotelAddress": "Chandevadi Road | Varande Village, Dandeli, India",
        "city": "UTTARKANNADA"
    }

    ,
    {
        "hotelName": "Citrus Karwar",
        "hotelAddress": "Near Canara Bank, Kaikani Road, Karwar 581301, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Banana County Resort",
        "hotelAddress": " Karwar | Yellapur, Karwar 581359, India",
        "city": "UTTARKANNADA"
    },
    {
        "hotelName": "Panther Jungle Stay",
        "hotelAddress": "Near Badkanshida Village, Dandeli 581325, India",
        "city": "UTTARKANNADA "
    }

    ,
    {
        "hotelName": "Camp Riveredge Paradise",
        "hotelAddress": "Hankon Village | Karwar Taluka, Karwar 581356, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Dhenu Atithya",
        "hotelAddress": "Main Road | Opp Bus Stand, Murdeshwar 581350, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "RNS Guest House",
        "hotelAddress": "Gandhibazar Main Road | Bhatkal Taluk, Murdeshwar 581 350, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Madhuvana Jungle Cottage",
        "hotelAddress": "Dandeli 581325, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Aditi Comforts Service Apartment Hotel",
        "hotelAddress": "Green Street, near Shivaji Circle, Karwar 581301, India",
        "city": "UTTARKANNADA"
    },

    {
        "hotelName": "Naveen Beach Resort",
        "hotelAddress": "Bhatkal Taluk, Murdeshwar 581 350, India",
        "city": "UTTARKANNADA"
    },




    {
        "hotelName": "Summer Sands Beach Resort",
        "hotelAddress": "Chota Mangalore,Ullal, Mangalore 575020, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "Om Beach Resort",
        "hotelAddress": "Om Beach Road,Bangle Gudde, Gokarna 581326, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Arthigamya Resort & Spa",
        "hotelAddress": "Om Beach Road, Kudle Beach, Gokarna 581320, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Sea Bird Holiday Resort",
        "hotelAddress": "Bangle Gudda, | Gokarn - 581 326 Tq Kumta, Uttar Kannada Dist, Gokarna 581326, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "SwaSwara",
        "hotelAddress": "Om Beach, Donibhail | Gokarna, Gokarna 581326, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Koffeewoods Homestay",
        "hotelAddress": "Kelahalli Estate | Balur Post, Mudigere, Chikkamagalur, India",
        "city": "DHAKSHANAKANNADA"
    },
    {
        "hotelName": "Stone Valley Resorts Sakleshpur",
        "hotelAddress": "Stone Valley Road | Bilisare, Devalkere post, Sakleshpur Taluk, Hassan, Sakleshpur 573165, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "The Serai",
        "hotelAddress": "B.M. Road | Mugthihalli Post, Chikkamagalur 577133, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "Gokulam Nalanda Resort",
        "hotelAddress": "NH 17, Jn. Nileshwar, Kasargod District, Nileshwar 671314, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "The Windflower Resort and Spa, Coorg",
        "hotelAddress": "S.No.202/1, Kedakal Village | Suntikoppa Hobli, Somwarpet Taluk, Suntikoppa 571237, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Neeleshwar Hermitage",
        "hotelAddress": "P.O. Ozhinhavalappu, Kasaragod 671314, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "Good Earth Homestay",
        "hotelAddress": "Aradhana Vijayapura, Chikkamagalur 577101, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Flameback Lodges",
        "hotelAddress": " Billur Post, Pattadur Village, Mudigere 577132, India",
        "city": "DHAKSHANAKANNADA"
    },
    {
        "hotelName": "Kalgreen Valley Resort",
        "hotelAddress": "Kalsapur Estate, Koppa Taluk, Chikkamagalur 577126, India",
        "city": "DHAKSHANAKANNADA"
    }

    ,
    {
        "hotelName": "Leisure Vacations Down's Retreat",
        "hotelAddress": "| Galibeedu Village, Madikeri 571201, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Club Mahindra Madikeri, Coorg",
        "hotelAddress": "Galibeedu Road, Village Kalakeri Nidugane, Madikeri 571204, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "The Gateway Hotel Old Port Rd Mangalore",
        "hotelAddress": "Old Port Road | Opposite to the District Commissioner's Office, Mangalore 575001, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Kudle Beach View Resort & Spa",
        "hotelAddress": " | Kudle Beach, Gokarna 581326, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "The Ocean Pearl",
        "hotelAddress": "Navabharath circle | Kodialbail, Mangalore 575003, India",
        "city": "DHAKSHANAKANNADA"
    },

    {
        "hotelName": "Vijay Comforts Hotel",
        "hotelAddress": "45/4&5 Near Kumaradhara River, Subramanya 574238, India",
        "city": "DHAKSHANAKANNADA"
    },




    {
        "hotelName": "Lords Eco Inn Mysuru Road Bengaluru",
        "hotelAddress": "Mysore Road | No.5, Jyoti Nagar, Near Nayandahalli Circle, Bengaluru 560039, India",
        "city": "RAMNAGARA"
    }

    ,
    {
        "hotelName": "Treebo Dwaraka Grand",
        "hotelAddress": "Sri Manjunath Arcade No 55 Kumarswamy Layout Govinayakanahalli, Bengaluru 560078, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Sunray Hotel",
        "hotelAddress": "Opp Prestige Tech Park, Marathahalli Outer Ring Road, Kadubesanahalli, | Marathahalli, Opp. Prestige Tech Park, Bengaluru 560087, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Nandhini Hotel - J.P.Nagar",
        "hotelAddress": "726/A 24'th Main Road | 6th Phase, J.P. Nagar, Bengaluru 560078, India",
        "city": "RAMNAGARA"
    }

    ,
    {
        "hotelName": "Bannerghatta Nature Camp",
        "hotelAddress": "Bannerghatta-Kagalipura Road | Near Hakki-Pikki Colony, Bhoothanahalli 560083, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Canary Sapphire - CRN",
        "hotelAddress": "#274 S C Road | Gandhi Nagar, Bengaluru 560009, India",
        "city": "RAMNAGARA"
    },
    {
        "hotelName": "Suraksha Park",
        "hotelAddress": "Shivaji Towers, No. 92, J C Road, Bengaluru, India",
        "city": "RAMNAGARA"
    }

    ,
    {
        "hotelName": "UG Regal",
        "hotelAddress": "453 / 1 O T C Road, Bengaluru 560053, India",
        "city": "RAMNAGARA"
    }

    ,
    {
        "hotelName": "Shelton Grand",
        "hotelAddress": "73 M G Road, Bengaluru 560001, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Polo Nest Service Apartments",
        "hotelAddress": "490 West Wing 8'th Main Road, Bengaluru 560071, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Shiva Ganga Hotel",
        "hotelAddress": "186 Balepete Main Road | Near Majestic Theatre, Balepet, Bengaluru 560053, India",
        "city": "RAMNAGARA"
    }

    ,
    {
        "hotelName": "Park Hotel and Resort",
        "hotelAddress": "No 23/2b | opposite BMTC Bus stand, Bannerghatta National Park,, Bengaluru 560083, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Holiday Village",
        "hotelAddress": " #35 9th Mile | Kanakapura Road, Bengaluru 560062, India",
        "city": "RAMNAGARA"
    },
    {
        "hotelName": "OYO 10218 Hotel Berry's",
        "hotelAddress": "36/1 Outer Ring Road | Kadubesanahalli, Bengaluru 560060, India",
        "city": "RAMNAGARA"
    },
    {
        "hotelName": "OYO 9026 near Mysore Road",
        "hotelAddress": "ollege Post, Rv Vidya Niketan, Mailasandra, Bengaluru 560059, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Hotel Kadamba Guestline",
        "hotelAddress": "Kengeri Mysore Road, Bengaluru 560068, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "The Country Club Mysore Road",
        "hotelAddress": "103/17 Kumbulgudu Village Kengeri Hobli Sy No | Bangalore South Taluk, Bengaluru 560074, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "The Garden Asia Resorts and Hotel",
        "hotelAddress": "No 40/2 Thagachuguppe Main Road | Kumbalgodu, Mysore Road, Bengaluru 560074, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Guhantara",
        "hotelAddress": "Sy. No. 177 & 177/18 Nowkalpalya, Kaggalipura, Bengaluru 560068, India",
        "city": "RAMNAGARA"
    },

    {
        "hotelName": "Wonderla Resort",
        "hotelAddress": "28th KM, Mysore Road, Bengaluru 562109, India",
        "city": "RAMNAGARA"
    },





    {
        "hotelName": "SLV Lodging & Boarding",
        "hotelAddress": "Chittapur Road | near Subash Chowk, Yadgir 585201, India",
        "city": "YADGIR"
    }

    ,
    {
        "hotelName": "Hotel Pariwar",
        "hotelAddress": "No 1 - 73/1 Station Road | Opposite To Kptcl, Gulbarga 585102, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "The Central Park Hotel",
        "hotelAddress": "Opp. Gescom,Station Road, Gulbarga 585102, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Hotel Heritage Inn",
        "hotelAddress": "42 A SB Temple Road, Gulbarga 585101, India",
        "city": "YADGIR"
    }

    ,
    {
        "hotelName": "Hotel Meridian Inn",
        "hotelAddress": "8-1305/66/B, Humnabad Highway | Kapnoor Area, Gulbarga 585104, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Rest Inn Guest House",
        "hotelAddress": "Upstairs KK Sports, Jail Road, near DDPI office, Raichur 584101, India",
        "city": "YADGIR"
    },
    {
        "hotelName": "Atharva Hotel",
        "hotelAddress": "Ram Mandir Circel.High Court Rood-Gulbarga, Gulbarga 585105, India",
        "city": "YADGIR"
    }

    ,
    {
        "hotelName": "Hotel City Park",
        "hotelAddress": "Timmapuri Circle | Station Road, Gulbarga 585102, India",
        "city": "YADGIR"
    }

    ,
    {
        "hotelName": "Hotel Mathura",
        "hotelAddress": "Station Main Road | Kandoor Mall, 3rd floor, S V P Circle, Gulbarga 585102, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Hotel Ashraya Comforts Gulbarga",
        "hotelAddress": "Kadechur Central, opp City Municipal Corporation, Main Road | Jagat Circle, Gulbarga, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Krown Plaza Deluxe Lodge",
        "hotelAddress": "GDA Layout, Near Central Bus Stand, M S K Mill Road, Gulbarga 585103, India",
        "city": "YADGIR"
    }

    ,
    {
        "hotelName": "Anasuya Datta Lodge",
        "hotelAddress": "Main Road Ganagapur, Gulbarga 505102, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Sun International Hotel",
        "hotelAddress": "72,Jewargi Road, Near Bhagyawanti Nagar, Gulbarga, Karnataka, Gulbarga, India",
        "city": "YADGIR"
    },
    {
        "hotelName": "Hotel Kadamba Residency",
        "hotelAddress": "Dr Jawali Complex, Super Market, Gulbarga, India",
        "city": "YADGIR "
    }

    ,
    {
        "hotelName": "Slv Tourist Lodge",
        "hotelAddress": "Station Road, Raichur, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Mohiuddin Residency",
        "hotelAddress": "Bus Station Road, Raichur 584101, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Brindavana Lodge",
        "hotelAddress": "Jail Road, Raichur 584101, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Hotel Darshan Lodging",
        "hotelAddress": "Main Road, Gulbarga 585102, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Sai Datta Lodge",
        "hotelAddress": "Main Road Ganagapur, Gulbarga 585212, India",
        "city": "YADGIR"
    },

    {
        "hotelName": "Kapila Lodge",
        "hotelAddress": "Msk Mill Road, Opp Central Bus Stand, Gulbarga 585103, India",
        "city": "YADGIR"
    },





    {
        "hotelName": "Rajathadri Hill Villa",
        "hotelAddress": "Temple Road BR Hills | B.R.Hills, Chamarajanagar 571 441, India",
        "city": "CHAMARAJANAGAR"
    }

    ,
    {
        "hotelName": "Cofiya Lodge",
        "hotelAddress": "Calicut Road | Maharaja Complex, Gundlupet 571111, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Nijaguna Resorts & Spa",
        "hotelAddress": "Mysuru-Ooty Road | Panjanahalli Village, Near Bandipur Wildlife Sanctuary, Gundlupet 571123, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "La Flora Sharmada Hidden Nest",
        "hotelAddress": "Belavadi Road, Gundlupet 571111, India",
        "city": "CHAMARAJANAGAR"
    }

    ,
    {
        "hotelName": "Jaladhama Resort",
        "hotelAddress": "Mudukuthore, Talakadu | Narasipura Taluk, Talakad 571112, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Naughty Club Resort",
        "hotelAddress": "Old Asanoor | Dharani Gardens, Sathyamangalam 638401, India",
        "city": "CHAMARAJANAGAR"
    },
    {
        "hotelName": "MC Resort Wildlife Resort Bandipur",
        "hotelAddress": "Bangalore - Ooty Road | Melukamanahalli Near Bandipur National Park Hangala Post Gundlupet Taluk, Bandipur 571126, India",
        "city": "CHAMARAJANAGAR"
    }

    ,
    {
        "hotelName": "The Serai Bandipur",
        "hotelAddress": "Kaniyanpura Village, Mangala Post | Gundlupet Taluk, Chamarajanagar, Bandipur 571126, India",
        "city": "CHAMARAJANAGAR"
    }

    ,
    {
        "hotelName": "Indus Valley Ayurvedic Centre",
        "hotelAddress": "Lalithadripura Rd | Ittigegud, Mysuru (Mysore) 570 028, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Lalitha Mahal Palace Hotel",
        "hotelAddress": "106, Nanjungud Road,, Mysuru (Mysore) 570004, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "The Village",
        "hotelAddress": "GDA Layout, Near Central Bus Stand, M S K Mill Road, Gulbarga 585103, India",
        "city": "CHAMARAJANAGAR"
    }

    ,
    {
        "hotelName": "The Windflower Resort & Spa, Mysore",
        "hotelAddress": "Maharanapratap Road, Mysuru (Mysore) 570010, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Mysore Chitravana Resort",
        "hotelAddress": "#39 Manandawady Road, Mysuru (Mysore) 570008, India",
        "city": "CHAMARAJANAGAR"
    },
    {
        "hotelName": "Marvakandy Resort The Big Cat Den",
        "hotelAddress": "| Behind Govt. Silk Farm Masinagudi, Masinagudi 643223, India",
        "city":" CHAMARAJANAGAR "
    }

    ,
    {
        "hotelName": "Hotel Mayura Yatrinivas Mysore",
        "hotelAddress": "	2, Jhansi Laxmi Bai Road, Mysuru (Mysore) 570005, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Radisson Blu Plaza Hotel Mysore",
        "hotelAddress": "1 MG Road,, Mysuru (Mysore) 570010, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Dasharath Hotel",
        "hotelAddress": " 1159, Ramsons House | Near Zoo Garden, Mysuru (Mysore) 570010, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Bamboo Banks Farm Guest House",
        "hotelAddress": " Masinigudi Village | The Nilgiris, Masinagudi 643223, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Sai Datta Lodge",
        "hotelAddress": "Main Road Ganagapur, Gulbarga 585212, India",
        "city": "CHAMARAJANAGAR"
    },

    {
        "hotelName": "Safari Land Farm & Guest House",
        "hotelAddress": "Safari Land Safari Land Farm & Guest House 4/78k | Nilgiri District, Masinagudi 643223, India",
        "city": "CHAMARAJANAGAR"
    },



]
var features=["free wifi","Wi-Fi/high-speed Internet","Spa featuring body treatments and scrubs"
    ,"Fitness Centre open 24 hours","Outdoor swimming pool","24-hour Room Service","Currency exchange",
    "Doctor on call","Valet car parking","24-hour laundry and dry-cleaning services",
    "Babysitter on request","Secretarial services","Courier services"];

var food=["free breakfast","free meal","free diner","free welcome drink"]
var Currency=["INR- indian rupees"]
var discountPrice=[2500,3000,2000,1000,850,250,4500]

function generateHotelDetails(){

for (var p=0;p<hotelDetails.length;p++){
    var detailsObj = new hotelDetailsDB();
    console.log(hotelDetails[p]);
    var obj={}
    obj.Hotel=hotelDetails[p].hotelName
    obj.features= features[Math.round(Math.floor(Math.random() * 7) + 0)]
    obj.food=food[Math.round(Math.floor(Math.random() * 2) + 0)];
    obj.Currency=Currency[0];
    obj.discountPrice=discountPrice[Math.round(Math.floor(Math.random() * 4) + 0)];
    obj.city=hotelDetails[p].city;
    obj.hotelAddress=hotelDetails[p].hotelAddress;
    detailsObj.details=obj
    detailsObj.save(function(err,result){
        if(err)
            console.log(err)
        console.log("Credencials generated")
        process.exit(0);
    })
}


};

generateHotelDetails()
