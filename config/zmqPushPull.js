/**
 * Created by rranjan on 11/29/15.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
   /* SocketIo = require('./socketIo').getSocketIoServer()*/

    dataPushHandlers = require("../app/dataHandler");
var zmq = require('zmq');

var zmqPushPull = function (config) {

    startImagePulling()
    function startImagePulling() {
        var zmqSocket = zmq.socket('pull');
        zmqSocket.connect('tcp://127.0.0.1:5211');
        console.log('Subscriber connected to port 5211');
        zmqSocket.on('message', function (message) {
           /* console.log("gggggg");
            console.log(message)*/
            processImageData(message,"cameraImage")

        });
    }


    function processImageData(message,portName) {
       /* var handlerObjectName = portName + "Handler";*/
        var handler = dataPushHandlers["cameraImageInfoDataHandler"];
        handler.pushCameraData(message);
    }




};


module.exports = {zmqPushPull: zmqPushPull};






